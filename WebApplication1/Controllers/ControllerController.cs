﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class ControllerController : Controller
    {
        // GET: Controller
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page";

            return View();
        }

        public ActionResult Data()
        {
            ViewData["school"] = "靜宜大學";

            return View();
        }

        public ActionResult Bag()
        {
            ViewBag.phone = "04-0000000";

            return View();
        }
        public ActionResult BMI()
        {

            return View();
        }
        [HttpPost]
        public ActionResult BMI(float hh, float ww)
        {
            float m_hh = hh / 100;
            float bmi = ww / (m_hh * m_hh);
            string level = "";
            if (bmi < 18.5)
            {
                level = "體重過輕";
            }
            else if (18.5 <= bmi && bmi < 24)
            {
                level = "正常範圍";
            }
            else if (24 <= bmi && bmi < 27)
            {
                level = "過重";
            }
            else if (27 <= bmi && bmi < 30)
            {
                level = "輕度肥胖";
            }
            else if (30 <= bmi && bmi < 35)
            {
                level = "中度肥胖";
            }
            else if (35 <= bmi)
            {
                level = "重度肥胖";
            }

            ViewBag.Result = bmi;
            ViewBag.level = level;

            return View();
        }

    }
}